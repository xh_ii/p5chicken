
//NOT YET A GAME                
//Bennett Foddy, NYU Game Center, 2015-2017

//Your assignment: take this project further and make it into a game.
//You should be able to do this without using much more of p5.play's functionality than what is already here:
//Sprites, Animations Groups, Sounds, Input, Text and Overlap. 

//global variables that hold the images for the chicken and eggs
//we need these so we can refer to them after the files are loaded
//Because we're declaring them outside all the functions, they are 'global' meaning they can be
//used anywhere. A variable declared inside a function or a loop is 'local' meaning it can only be used in that scope.

var frameCount;

var states = 
{
	pause: 0,
	over: 1,
	clear: 2,
	laying: 3,
	hatching: 4,
	fleeing: 5,
	racing: 6,
	beforeOver: 7,
	beforeClear: 8
};
var gameState;

var preSecond;
var passingTime;

var bg00Img;
var bg0Img;
var bg1Img;
var bg2Img;
var bg3Img;
var bg4Img;
var bg5Img;
var bg6Img;
var bg7Img;
var bg8Img;
var cloudImg;

var standingImg;
var sittingImg;
var chickStandingImg;
var eggTopImg;
var eggBottomImg;
var qButtonImg;
var wButtonImg;
var eButtonImg;

var chichickenImg;
var piajiImg;
var clockImg;
var clockminus5Img;

var clock;
var clockminus5;

var grassAnimation;
var walkAnimation;
var chickFleeAnimation;

//variables to hold the sounds
var hatchSound;
var laySound;
var hitSound;
var notHitSound;
var timeSound;
var clearSound;
var rockSound;
var failSound;

var bgs;
var sky;
var cloud;

//a variable for the chicken (which is the player)
var chicken;

//var to refer to the group of egg sprites
var eggs;
var chick;
var chichiken;
var piaji;

//nice pixel font
var pixelFont;

// button
var buttons;
var items;

var beforeTimer;

var overReason;

function randomButtonCD() 
{
	return -(random(10) + 10);
}

function randomButtonTime()
{
	return (random(50) + 40);
}

function createRandomButton()
{
	var button = createSprite(width/2, 0.75 * height);
	button.type = random(3);
	button.timer = randomButtonTime();
	switch(parseInt(button.type))
	{
		case 0:
			button.addImage(qButtonImg);
			break;
		case 1:
			button.addImage(wButtonImg);
			break;
		case 2:
			button.addImage(eButtonImg);
			break;
	}

	buttons.add(button);
	//console.log("type: %d", type);	
}

function randomItemCD() 
{
	return -(random(60) + 60);
}

function clearClock()
{
	preSecond = second();
	passingTime = 0;
}

function countClock()
{
	var s = second();
	if (parseInt(s) != parseInt(preSecond))
	{
		passingTime = passingTime + 1;
		preSecond = s;
	}
}

function buttonHit()
{
	chicken.dashTimer = 6;
	chicken.velocity.x = -10;

	hitSound.play();

	// cooling down
	chicken.buttonCDTimer = randomButtonCD();
	if (buttons.length > 0)
	{
		buttons.remove(buttons[0]);
	}
}

function buttonGone()
{
	notHitSound.play();

	// cooling down
	chicken.buttonCDTimer = randomButtonCD();
	if (buttons.length > 0)
	{
		buttons.remove(buttons[0]);
	}
}

function gamePause()
{
	gameState = states.pause;
	updateSprites(false);
}

function gameResume()
{
	gameState = states.racing;
	updateSprites(true);
}

function gameOver()
{
	gameState = states.over;
	failSound.play();
}

function gameClear()
{
	gameState = states.clear;
	clearSound.play();
}

function gameBeforeOver()
{
	gameState = states.beforeOver;
	updateSprites(false);
}

function gameBeforeClear()
{
	gameState = states.beforeClear;
	updateSprites(false);
}

function newGame()
{
	setup();

	chick.remove();

	updateSprites(true);
}


//here we are going to load all the files and store them in the variables
//it's advisable to do this in the preload() function, which runs before the game starts
function preload() 
{
	bg00Img = loadImage("assets/bg00.png");
	bg0Img = loadImage("assets/bg0.png");
	bg1Img = loadImage("assets/bg1.png");
	bg2Img = loadImage("assets/bg2.png");
	bg3Img = loadImage("assets/bg3.png");
	bg4Img = loadImage("assets/bg4.png");
	bg5Img = loadImage("assets/bg5.png");
	bg6Img = loadImage("assets/bg6.png");
	bg7Img = loadImage("assets/bg7.png");
	bg8Img = loadImage("assets/bg8.png");
	cloudImg = loadImage("assets/cloud.png");

	standingImg = loadImage("assets/chicken_walk1.png"); 
	sittingImg = loadImage("assets/chicken_sit.png"); 
	eggTopImg = loadImage("assets/egg_top.png"); 
	eggBottomImg = loadImage("assets/egg_bottom.png"); 
	chichickenImg = loadImage("assets/chichicken.png");
	piajiImg = loadImage("assets/chicken_piaji.png"); 
	clockImg = loadImage("assets/clock.png"); 
	clockminus5Img = loadImage("assets/clock-5.png"); 

	//you can load an animation by giving a sequence of images like this
	grassAnimation = loadAnimation("assets/bg0.png","assets/bg00.png","assets/bg0.png");
	grassAnimation.frameDelay = 15;

	walkAnimation = loadAnimation("assets/chicken_walk1.png","assets/chicken_walk2.png");
	walkAnimation.frameDelay = 12;

	chickFleeAnimation = loadAnimation("assets/chick_walk1.png","assets/chick_walk2.png");
	chickFleeAnimation.frameDelay = 6;

	qButtonImg = loadImage("assets/Q.png");
	wButtonImg = loadImage("assets/W.png");
	eButtonImg = loadImage("assets/E.png");
	obstacleImg = loadImage("assets/obstacle.png");
	
	hatchSound = loadSound("assets/crack.wav");
	laySound = loadSound("assets/squawk.wav");
	hitSound = loadSound("assets/Hit.wav");
	notHitSound = loadSound("assets/NotHit.wav");
	timeSound = loadSound("assets/timeDe.wav");
	clearSound = loadSound("assets/LevelWin.wav");
	rockSound = loadSound("assets/rock.wav");
	failSound = loadSound("assets/Fail.wav");

	pixelFont = loadFont('assets/arcadepix.ttf');
	
}

//the setup() function runs once, after all the files are loaded in preload()
//generally this is used to set up the game world and all the objects
function setup() {

	// create the canvas with the specficed size.
	var canvas = createCanvas(640,440); 
	canvas.parent("container"); //position the canvas within the html, look in the index.html file for the div called "container"

	frameCount = 0;
	beforeTimer = 0;

	bgs = new Group();

	var bg0 = createSprite(0, height/2);
	bg0.addAnimation("grass", grassAnimation);
	bg0.changeAnimation("grass");

	var bg1 = createSprite(0, height/2);
	var bg2 = createSprite(0, height/2);
	var bg3 = createSprite(0, height/2);
	var bg4 = createSprite(0, height/2);
	var bg5 = createSprite(0, height/2);
	var bg6 = createSprite(0, height/2);
	var bg7 = createSprite(0, height/2);
	
	bg1.addImage(bg1Img);	
	bg2.addImage(bg2Img);
	bg3.addImage(bg3Img);
	bg4.addImage(bg4Img);	
	bg5.addImage(bg5Img);
	bg6.addImage(bg6Img);
	bg7.addImage(bg7Img);	

	bg7.depth = 1;
	bg6.depth = 2;
	bg5.depth = 3;
	bg4.depth = 4;
	bg3.depth = 5;
	bg2.depth = 6;
	bg1.depth = 7;
	bg0.depth = 8;

	bgs.add(bg0);
	bgs.add(bg1);
	bgs.add(bg2);
	bgs.add(bg3);
	bgs.add(bg4);
	bgs.add(bg5);
	bgs.add(bg6);
	bgs.add(bg7);

	sky = createSprite(0, height/2);
	sky.addImage(bg8Img);

	cloud = createSprite(0, height/2);
	cloud.addImage(cloudImg);

	//create an empty group to hold all the eggs. For now we don't need to add anything to it.
	//In p5.play, a group is just an array with some convenient methods for collision and drawing
	eggs = new Group(); 

	//For the player, create an empty sprite in the middle of the screen.
	chicken = createSprite(width / 2, height / 2); 

	//Now we add the different animation frames to it. The first parameter in quotes
	//is the animation's name, which we will use to change from one animation to another.
	//The second parameter is the variable we stored the animation image in.
	chicken.addImage("standing", standingImg); 
	chicken.addImage("sitting", sittingImg);
	chicken.addAnimation("walking",walkAnimation);

	//the chicken should start in the standing position
	chicken.changeAnimation("standing");
	chicken.setVelocity(0, 0);
	chicken.setCollider("rectangle", 0, 30, 65, 15);

	chicken.eggTimer= 0;
	chicken.dashTimer = 0;
	chicken.buttonCDTimer = randomButtonCD();
	chicken.itemCDTimer = randomItemCD();

	chichicken = createSprite(width / 4 + 60, height / 2 + 10); 
	chichicken.addImage(chichickenImg);

	piaji = createSprite(width / 4 + 60, height / 2 + 10); 
	piaji.addImage(piajiImg);

	clock = createSprite(16, height - 20); 
	clock.addImage(clockImg);

	items = new Group();
	buttons = new Group();

	//set the font and fontsize
	textFont(pixelFont);
	textSize(10);	

	gameState = states.laying;

	clearClock();
}

function update(){
	++frameCount;

	cloud.velocity.x = 0.2;
	if (cloud.position.x >= width)
	{
		cloud.position.x = 0;
	}

	//most game engines have an update() function that is called 60 times per second. 
	//Now, p5.play DOESN'T have an update function built in. 
	//I just made a function called update that I call from draw() to keep the logic and drawing separate.
	//It's just neater, and it makes our code a little more like it will look in other game engines.
	
	//1. PROCESSING PLAYER INPUT
	//The three functions you want to know about are keyDown(), keyWentDown(), and keyWentUp()
	//keyWentDown() is true just for one frame after the key is pressed. It's good for one-time actions like jumping. 
	//keyWentUp() is true for just one frame, as soon as you release a key. It's good for things like charging a shot.
	//keyDown() stays true as long as you hold the key. It's great for things like movement.
	
	if (gameState == states.laying)
	{
		/*
		if(keyDown("LEFT")){
			chicken.velocity.x = -3; //if I'm holding the left key, set the chicken's horizontal velocity to -3
			chicken.mirrorX(1); //flip the sprite to face left
		} 
		else if (keyDown("RIGHT")){
			chicken.velocity.x = 3; //if I'm holding the right key, set the chicken's horizontal velocity to 3
			chicken.mirrorX(-1); //flip the sprite to face right
		}
		else {
			chicken.velocity.x = 0; //if I'm not holding left or right, don't move in the horizontal axis
		}

		//same again for vertical directions
		if(keyDown("UP")){
			chicken.velocity.y = -3;
		} 
		else if (keyDown("DOWN")){
			chicken.velocity.y = 3;
		}
		else {
			chicken.velocity.y = 0;
		}
		*/

		//now that we've set the velocity, we can talk about what should happen when the bird is moving or standing still
		if ((Math.abs(chicken.velocity.x) > 0 ) || (Math.abs(chicken.velocity.y) > 0)){
			chicken.changeAnimation("walking"); //if we're moving, play the walking animation
		}
		else {
			chicken.changeAnimation("standing"); //play the standing still animation

			//if we're standing still, we might want the chicken to be able to lay an egg!
			//first check we're not sitting on an egg!

			//this goes through all the eggs, and returns true if any egg is overlapping the chicken
			if (eggs.overlap(chicken)==false){ 

				if (keyDown("SPACE")){ 

					chicken.changeAnimation("sitting"); //sit the chicken down
					chicken.eggTimer = chicken.eggTimer + 1; //we're going to count how many frames we've been sitting down
					if (chicken.eggTimer > 30){
						//just as an example, let's print a message to the console. These are useful for debugging!
						console.log("laying an egg!")

						//we've been sitting for 30 frames. make an egg!
						//the egg consists of two sprites, for the top and bottom half
						var newEggTop = createSprite(chicken.position.x, chicken.position.y + 20);
						newEggTop.addImage(eggTopImg);
						
						//I need to keep track of how long the egg has been around so we can know when it should hatch.
						//To do that I'm going to add a variable to the sprite called hatchTimer
						//random(x,y) returns a random decimal number between x and y, but we want whole numbers here so
						//I'm using the Math.round function to round the number to an integer.
						newEggTop.hatchTimer = Math.round( random(60,140) ); 
						newEggTop.hatchDirection = -1; //which way the egg will move when it hatches
						eggs.add(newEggTop); //add the egg section to the group of eggs

						var newEggBottom = createSprite(chicken.position.x, chicken.position.y + 20);
						newEggBottom.addImage(eggBottomImg);
						newEggBottom.hatchTimer = newEggTop.hatchTimer;
						newEggBottom.hatchDirection = 1;
						eggs.add(newEggBottom);
						
						laySound.play();

						chicken.eggTimer = 0; //reset the timer so we can't lay another egg too soon

						gameState = states.hatching;
					}

				}
				else {
					chicken.eggTimer = 0; //we're not sitting down anymore... reset the timer
				}

			}
		}

	}
	else
	if (gameState == states.hatching)
	{
		//2. PROCESSING EGG BEHAVIOR

		//handle egg hatching!
		if (eggs.length == 0)
		{
			gameState = states.fleeing;
			return;
		}
		for (var i=0; i<eggs.length; i++){ //we're iterating over all the eggs in the array
			var thisEgg = eggs[i]; //get a reference to the egg at index i
			thisEgg.hatchTimer = thisEgg.hatchTimer - 1; //count down the egg's timer until it hatches by 1
					
			if ((thisEgg.hatchTimer < 20)&&(thisEgg.hatchTimer > 0)){
				//the egg is almost ready to hatch! let's make it rattle a bit by moving it left and right
				//the modulo operator '%' gives you the remainder after dividing by that number, so:
				//5 % 2 = 1, 4 % 2 = 0, etc. It's a useful way to find out if a number is even or odd.
				//this line returns true for one frame and then false for one frame in a loop
				if (thisEgg.hatchTimer % 2 == 0){
					thisEgg.position.x = thisEgg.position.x + 2;
				}
				else {
					thisEgg.position.x = thisEgg.position.x - 2;
				}
			}	

			//when the timer gets to zero, we'll play a sound
			if (thisEgg.hatchTimer ==0){
				hatchSound.play();
				chick = createSprite(thisEgg.position.x, thisEgg.position.y);
				chick.addAnimation("fleeing", chickFleeAnimation);
				chick.changeAnimation("fleeing");
				chick.fleeingTimer = 0;
			}

			if (thisEgg.hatchTimer <= 0){
				//hatch the egg! This will just mean moving the two halves of the egg away from each other
				thisEgg.position.y += 10*thisEgg.hatchDirection;

				//NOTE: Something else should happen here, right? You can't have a game where
				//eggs hatch and nothing comes out.
			}
			if (thisEgg.hatchTimer <= -10){
				//now that we've pushed the two halves apart, we can destroy them.
				eggs.remove(thisEgg);
			}		
		}
	}
	else 
	if (gameState == states.fleeing)
	{
		if (chick.fleeingTimer <= -13)
		{
			chick.velocity.x = 0;
			gameState = states.racing;
			startTime = second();
			chicken.changeAnimation("walking");
			chicken.mirrorX(1);
		}
		else
		{
			chicken.velocity.x = 10;
			chick.velocity.x = -20;
			chick.fleeingTimer = chick.fleeingTimer - 1;
		}
	}
	else
	if (gameState == states.racing)
	{
		countClock();

		// menu
		if (keyDown("ESC"))
		{
			gamePause();
		}

		if (chicken.collide(chick))
		{
			gameBeforeClear();
			//gameState = states.beforeClear;
			//gameClear();
		}

		if (chicken.position.x >= 680)
		{
			overReason = "- Chick Lost -"
			gameBeforeOver();
			//gameState = states.beforeOver;
			//gameOver();
		}

		// fake 3D background	
		var speed = 0.1;
		for (var i = 0; i < bgs.length; ++i)
		{
			var bg = bgs[i];

			bg.velocity.x = speed;
			speed = speed + 0.12;
			
			if (bg.position.x >= width)
			{
				bg.position.x = 0;
			}
		}

		bgs[bgs.length - 1].velocity.x = 2;
		/*
		bgs[1].velocity.x = 0;
		bgs[1].velocity.y = 0.08 * Math.sin(frameCount / 40);
		*/

		//same again for vertical directions
		if (keyDown("UP"))
		{
			if (chicken.position.y <= 80)
			{
				chicken.velocity.y = 0;
			}
			else
			{
				chicken.velocity.y = -3;
			}
		} 
		else 
		if (keyDown("DOWN"))
		{
			if (chicken.position.y >= 360)
			{
				chicken.velocity.y = 0;
			}
			else
			{
				chicken.velocity.y = 3;
			}
		}
		else 
		{
			chicken.velocity.y = 0;
		}
		
		// keep in screen
		if (chicken.position.x <= 40 )
		{
			chicken.velocity.x = 10;
		}

		// random item
		if (parseInt(chicken.itemCDTimer) < 0)
		{
			chicken.itemCDTimer = chicken.itemCDTimer + 1;
			//console.log("%d", chicken.obstacleCDTimer);
			if (parseInt(chicken.itemCDTimer) >= 0)
			{
				var key = random(5);
				var item = createSprite(0, height * (random(7) + 2.5) / 10);

				if (parseInt(key) < 4)
				{
					// obstacle
					item.addImage(obstacleImg);
					item.setCollider("circle", 0, 0, 15);
					item.type = 0;				
				}
				else
				{
					// clock -5
					item.addImage(clockminus5Img);
					item.setCollider("rectangle", 0, 0, 60, 48);
					item.type = 1;
				}

				item.timer = 300;
				item.velocity.x = 2;
				items.add(item);

				chicken.itemCDTimer = randomItemCD();
			}		
		}

		for (var i = 0; i < items.length; ++i)
		{
			var item = items[i];
			if (item.position.x >= 660)
			{
				items.remove(item);
			}

			// run into obstacles
			if (chicken.overlap(item))
			{
				if (parseInt(item.type) == 0)
				{
					overReason = "- Rock Hit -";
					rockSound.play();
					gameBeforeOver();
				}
				else
				{
					passingTime = passingTime - 5;
					items.remove(item);
					timeSound.play();
				}
				//gameState = states.beforeOver;
				//gameOver();
			}
		}

		// random buttons
		for (var i = 0; i < buttons.length; ++i)
		{
			var button = buttons[i];
			// button already here
			if (parseInt(button.timer) > 0)
			{
				button.timer = button.timer - 1;
				if (parseInt(button.timer) <= 0)
				{
					buttonGone();
				}
				//console.log("%d", buttonTimer);
			}
		}
		// no button yet
		if (parseInt(chicken.buttonCDTimer) < 0)
		{
			chicken.buttonCDTimer = chicken.buttonCDTimer + 1;
			if (parseInt(chicken.buttonCDTimer) >= 0)
			{
				// generate a button
				createRandomButton();
				//console.log("spawn: %d", buttonTimer);	
			}
			//console.log("%d", buttonTimer);
		}

		// prevent cheating
		if (buttons.length > 0)
		{
			var button = buttons[0];
			if (keyDown("Q"))
			{
				if (parseInt(button.type) == 0)
				{
					buttonHit();
				}
				else
				{
					buttonGone();
				}
			}
			else
			if (keyDown("W"))
			{
				if (parseInt(button.type) == 1)
				{
					buttonHit();
				}
				else
				{
					buttonGone();
				}
			}
			else
			if (keyDown("E"))
			{
				if (parseInt(button.type) == 2)
				{
					buttonHit();
				}
				else
				{
					buttonGone();
				}
			}
		}

		if (parseInt(chicken.dashTimer) == 0)
		{
			chicken.velocity.x = 1;
		}
		else
		{
			chicken.dashTimer = chicken.dashTimer - 1;
		}

	}
	else
	if (gameState == states.pause)
	{
		preSecond = second();

		if (keyDown("ENTER"))
		{
			gameResume();		
		}
		else
		if (keyDown("R"))
		{
			newGame();		
		}
	}
	else
	if (gameState == states.over)
	{
		if (keyDown("R"))
		{
			newGame();		
		}
	}
	else
	if (gameState == states.clear)
	{
		if (keyDown("R"))
		{
			newGame();		
		}
	}
	else
	if (gameState == states.beforeClear)
	{
		beforeTimer = beforeTimer + 1;
		if (parseInt(beforeTimer) >= 40)
		{
			gameClear();
		}
	}
	else
	if (gameState == states.beforeOver)
	{
		beforeTimer = beforeTimer + 1;
		if (parseInt(beforeTimer) >= 40)
		{
			gameOver();
		}
	}
}

//The draw() function is called once a frame to draw all the things in the game.
//The order we draw them matters - newer things are drawn on top of older things
//So we start with the background
function draw() {
	//the background() function fills the background with a color. The color is given in web-style hex format
	//it's just three two-digit numbers stuck together, for the red, green and blue components.
	//the only catch it's they're hexadecimal numbers, which go 0123456789ABCDEF instead of 0123456789
	//so white would be 0xffffff, red would be 0xff0000, and blue would be 0x0000ff.
	background(0xffffff);	
	
	//here we call our custom update function to process input and movement
	update(); 
	
	drawSprite(sky);
	drawSprite(cloud);
	drawSprites(bgs);

	if (gameState == states.clear)
	{
		drawSprite(chichicken);
	}
	else
	if (gameState == states.over)
	{
		drawSprite(piaji);
	}
	else
	{
		drawSprites(items);

		drawSprite(chicken);

		//draw all the sprites in the eggs group. 
		//We want these to appear behind the chicken, so we draw them first
		drawSprites(eggs); 

		drawSprite(chick);

		drawSprites(buttons);
	}

	if (gameState == states.pause || gameState == states.racing)
	{
		drawSprite(clock);
	}

	//draw some text, to show the number of eggs
	fill(100, 100, 100);//change the color of the text to light yellow
	//when we print text we can put numbers in the text just by adding the number to the text.
	//the last two parameters of the text() function are just the coordinates on the canvas.	

	if (gameState == states.laying)
	{
		textSize(13);
		textAlign(CENTER);
		text("Hold SPACE to lay an egg.", width / 2, height / 2 + 130);
		text("Press UP DOWN to move.", width / 2, height / 2 + 150);
		text("Press Q W E to dash.", width / 2, height / 2 + 170);
		textSize(30);
		text("Naughty Chick", width / 2, height / 2 + 95);
	}
	else
	if (gameState == states.racing)
	{
		textSize(11);
		textAlign(LEFT);
		text("Passing Time: " + passingTime, 36, height - 10);	
		textAlign(RIGHT);
		text("Press ESC to pause.", width - 10, height - 10);	
	}
	else
	if (gameState == states.pause)
	{
		textSize(15);
		textAlign(CENTER);
		text("Game Paused", width / 2, height / 2);
		textSize(11);
		text("Press ENTER to continue.", width / 2, height / 2 + 30);
		text("Press R to restart.", width / 2, height / 2 + 50);
		textAlign(LEFT);
		text("Passing Time: " + passingTime, 36, height - 10);	
	}
	else
	if (gameState == states.over)
	{
		textSize(15);
		textAlign(CENTER);
		text("Game Over", width / 2  + 70, height / 2);
		textSize(11);
		text(overReason, width / 2  + 70, height / 2 + 30)
		text("Press R to restart.", width / 2  + 70, height / 2 + 50)
	}
	else
	if (gameState == states.clear)
	{
		textSize(15);
		textAlign(CENTER);
		text("Game Clear", width / 2  + 70, height / 2);
		textSize(11);
		text("Passing Time: " + passingTime, width / 2  + 70, height / 2 + 30);	
		text("Press R to restart.", width /2  + 70, height / 2 + 50)
	}
}


